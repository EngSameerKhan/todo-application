<?php

namespace App\Http\Controllers;

use App\ApiCode;
use App\User;
use Exception;


class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function login() {
        try {
            $credentials = request()->validate(['email' => 'required|email', 'password' => 'required|string|max:25']);
            if (! $token = auth()->attempt($credentials)) {
                return $this->respondUnAuthorizedRequest(ApiCode::INVALID_CREDENTIALS);
            }
            if (!auth()->user()->hasVerifiedEmail()) {
                return $this->respondBadRequest(ApiCode::EMAIL_ADDRESS_NOT_VERIFIED);
            }

            return $this->respondWithToken($token);
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }

    }

    private function respondWithToken($token) {
        return $this->respond([
            'token' => $token,
            'access_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ], "Login Successful");
    }


    public function logout() {
        try {
            auth()->logout();
            return $this->respondWithMessage('User successfully logged out');
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }

    }


    public function refresh() {
        return $this->respondWithToken(auth()->refresh());
    }

    public function me() {
        return $this->respond(auth()->user());
    }
}
