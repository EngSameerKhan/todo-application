<?php

namespace App\Http\Controllers;

use App\ToDo;
use Exception;
use Illuminate\Http\Request;
use App\Http\Requests\ToDoRequest;
class ToDoController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $user = auth()->user();
            if ($user) {
                return ToDo::paginate(10);
            } else {
                return response()->json([
                    'status' => 400,
                    'message' => 'There is no Such User Exist'
                ], 401);
            }
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(ToDoRequest $request)
    {
        try {
            $user = auth()->user();
            if ($user) {
                ToDo::create($request->getAttributes());
                return $this->respondWithMessage('Item Created Successfully');
            } else {
                return response()->json([
                    'status' => 400,
                    'message' => 'There is no Such User Exist'
                ], 401);
            }
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }

    }

    public function show($toDo)
    {
        try {
            $user = auth()->user();
            if ($user) {
                return ToDo::findOrFail($toDo);
            } else {
                return response()->json([
                    'status' => 400,
                    'message' => 'There is no Such User Exist'
                ], 401);
            }
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ToDo  $toDo
     * @return \Illuminate\Http\Response
     */
    public function edit(ToDo $toDo)
    {
        //
    }

    public function update(ToDoRequest $request, $toDo)
    {
        try {
            $user = auth()->user();
            if ($user) {
                $toDo=ToDo::findOrFail($toDo);
                $toDo->update($request->getAttributes());
                return $this->respondWithMessage("Item successfully updated");
            } else {
                return response()->json([
                    'status' => 400,
                    'message' => 'There is no Such User Exist'
                ], 401);
            }
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }

    }
    public function destroy($toDo)
    {
        try {
            $user = auth()->user();
            if ($user) {
                $toDo=ToDo::findOrFail($toDo);
                $toDo->delete();
                return $this->respondWithMessage("Item successfully Deleted");
            } else {
                return response()->json([
                    'status' => 400,
                    'message' => 'There is no Such User Exist'
                ], 401);
            }
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }

    }
}
