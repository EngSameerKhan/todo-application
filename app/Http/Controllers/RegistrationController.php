<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\User;
use Exception;

class RegistrationController extends Controller
{
    /**
     * Register User
     *
     * @param RegistrationRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function register(RegistrationRequest $request) {
        try {

            User::create($request->getAttributes())->sendEmailVerificationNotification();
            return $this->respondWithMessage('User successfully created');
        } catch (Exception $e) {
            return response()->json($e->getMessage());
        }

    }
}
